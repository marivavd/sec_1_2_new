import 'package:flutter/material.dart';
import 'package:sec_1_2_new/data/supabase.dart';
import 'package:sec_1_2_new/domain/controllers/password_controller.dart';
import 'package:sec_1_2_new/domain/internet.dart';
import 'package:sec_1_2_new/presentation/pages/log_in.dart';
import 'package:sec_1_2_new/presentation/pages/new_pass.dart';
import 'package:sec_1_2_new/presentation/pages/sign_up.dart';
import 'package:sec_1_2_new/presentation/widgets/dialogs.dart';
import 'package:sec_1_2_new/presentation/widgets/text_field.dart';

class Verify_OTP extends StatefulWidget {
  Verify_OTP({super.key, required this.email});

  String email;


  @override
  State<Verify_OTP> createState() => _Verify_OTPState();
}

class _Verify_OTPState extends State<Verify_OTP> {
  var controller = TextEditingController();


  @override
  Widget build(BuildContext context) {

    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 24),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 83,),
              Text(
                "Верификация",
                style: Theme.of(context).textTheme.titleMedium,
              ),
              SizedBox(height: 8,),
              Text("Введите 6-ти значный код из письма ", style: Theme.of(context).textTheme.titleSmall,),
              SizedBox(height: 28,),
              Custom_field(label: 'Код', hint: "", controller: controller),
              SizedBox(height: 48,),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  InkWell(
                    onTap: ()async{
                      checkInet(context, (){
                        send_email(
                            email: widget.email,
                            context: context,
                            onError: (String e){showError(context, e);},
                            onResponse: (){Navigator.push(context, MaterialPageRoute(builder: (_) => Verify_OTP(email: widget.email)));});
                      });
                    },
                    child: Text(
                      "Получить новый код",
                      style: Theme.of(context).textTheme.titleSmall!.copyWith(color: Color(0xFF7576D6)),
                    ),
                  )
                ],
              ),


              SizedBox(height: 449,),
              Align(
                alignment: Alignment.center,
                child: SizedBox(
                  height: 46,
                  width: 342,
                  child: FilledButton(
                    style: Theme.of(context).filledButtonTheme.style,
                    child: Text(
                      "Сбросить пароль",
                      style: Theme.of(context).textTheme.titleLarge,
                    ),
                    onPressed: ()async{
                      checkInet(context, () async {

                        await verify_OTP(email: widget.email, code: controller.text, onError: (String e){showError(context, e);}, onResponse:(){
                          Navigator.push(context, MaterialPageRoute(builder: (context) => New_pass()));
                        }, context: context);
                      });

                    },
                  ),
                ),
              ),
              SizedBox(height: 14,),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text("Я вспомнил свой пароль! ",style: Theme.of(context).textTheme.titleSmall!.copyWith(fontWeight: FontWeight.w400),),
                  InkWell(
                    child: Text("Вернуться", style: Theme.of(context).textTheme.titleSmall!.copyWith(color: Color(0xFF7576D6), fontWeight: FontWeight.w700),),
                    onTap: (){ Navigator.push(context, MaterialPageRoute(builder: (context) => Sign_In()));},
                  )
                ],
              )
            ],
          ),
        ),
      ),

    );
  }
}
