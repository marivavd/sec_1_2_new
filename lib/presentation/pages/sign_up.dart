import 'package:flutter/material.dart';
import 'package:sec_1_2_new/data/supabase.dart';
import 'package:sec_1_2_new/domain/controllers/password_controller.dart';
import 'package:sec_1_2_new/domain/internet.dart';
import 'package:sec_1_2_new/presentation/pages/log_in.dart';
import 'package:sec_1_2_new/presentation/widgets/dialogs.dart';
import 'package:sec_1_2_new/presentation/widgets/text_field.dart';

class Sign_Up extends StatefulWidget {
  const Sign_Up({super.key});


  @override
  State<Sign_Up> createState() => _Sign_UpState();
}

class _Sign_UpState extends State<Sign_Up> {
  var email_controller = TextEditingController();
  var password_controller = PasswordController();
  var confirm_password_controller = PasswordController();
  bool password_obscure = true;
  bool conf_password_obscure = true;


  @override
  Widget build(BuildContext context) {

    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 24),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 83,),
              Text(
                "Создать аккаунт",
                style: Theme.of(context).textTheme.titleMedium,
              ),
              SizedBox(height: 8,),
              Text("Завершите регистрацию чтобы начать", style: Theme.of(context).textTheme.titleSmall),
              SizedBox(height: 28,),
              Custom_field(label: 'Почта', hint: "***********@mail.com", controller: email_controller),
              SizedBox(height: 24,),
              Custom_field(label: 'Пароль', hint: "**********", controller: password_controller, is_obsure: password_obscure, tap_suffix: (){setState(() {
                password_obscure = !password_obscure;
              });},),
              SizedBox(height: 24,),
              Custom_field(label: 'Повторите пароль', hint: "**********", controller: confirm_password_controller, is_obsure: conf_password_obscure, tap_suffix: (){setState(() {
                conf_password_obscure = !conf_password_obscure;
              });},),
              SizedBox(height: 319,),
              Align(
                alignment: Alignment.center,
                child: SizedBox(
                  height: 46,
                  width: 342,
                  child: FilledButton(
                    style: Theme.of(context).filledButtonTheme.style,
                    child: Text(
                      "Зарегистрироваться",
                      style: Theme.of(context).textTheme.titleLarge,
                    ),
                    onPressed: ()async{
                      checkInet(context, () async {
                        if (password_controller.hash_pass() == confirm_password_controller.hash_pass()){
                          await sign_up(email: email_controller.text, password: password_controller.hash_pass(), onError: (String e){showError(context, e);}, onResponse:(){
                            Navigator.push(context, MaterialPageRoute(builder: (context) => Sign_In()));
                          }, context: context);
                        }
                        else{
                          showError(context, "Passwords don't match");
                        }
                      });
                      
                    },
                  ),
                ),
              ),
              SizedBox(height: 14,),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text("У меня уже есть аккаунт! ",style: Theme.of(context).textTheme.titleSmall!.copyWith(fontWeight: FontWeight.w400),),
                  InkWell(
                    child: Text("Войти", style: Theme.of(context).textTheme.titleSmall!.copyWith(color: Color(0xFF7576D6), fontWeight: FontWeight.w700),),
                    onTap: (){ Navigator.push(context, MaterialPageRoute(builder: (context) => Sign_In()));},
                  )
                ],
              )
            ],
          ),
        ),
      ),

    );
  }
}
