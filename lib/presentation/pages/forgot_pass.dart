import 'package:flutter/material.dart';
import 'package:sec_1_2_new/data/supabase.dart';
import 'package:sec_1_2_new/domain/controllers/password_controller.dart';
import 'package:sec_1_2_new/domain/internet.dart';
import 'package:sec_1_2_new/presentation/pages/log_in.dart';
import 'package:sec_1_2_new/presentation/pages/sign_up.dart';
import 'package:sec_1_2_new/presentation/pages/verify_otp.dart';
import 'package:sec_1_2_new/presentation/widgets/dialogs.dart';
import 'package:sec_1_2_new/presentation/widgets/text_field.dart';

class Forgot_Pass extends StatefulWidget {
  const Forgot_Pass({super.key});



  @override
  State<Forgot_Pass> createState() => _Forgot_PassState();
}

class _Forgot_PassState extends State<Forgot_Pass> {
  var email_controller = TextEditingController();


  @override
  Widget build(BuildContext context) {

    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 24),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 83,),
              Text(
                "Восстановление пароля",
                style: Theme.of(context).textTheme.titleMedium,
              ),
              SizedBox(height: 8,),
              Text("Введите свою почту", style: Theme.of(context).textTheme.titleSmall,),
              SizedBox(height: 28,),
              Custom_field(label: 'Почта', hint: "***********@mail.com", controller: email_controller),

              SizedBox(height: 503,),
              Align(
                alignment: Alignment.center,
                child: SizedBox(
                  height: 46,
                  width: 342,
                  child: FilledButton(
                    style: Theme.of(context).filledButtonTheme.style,
                    child: Text(
                      "Отправить код",
                      style: Theme.of(context).textTheme.titleLarge,
                    ),
                    onPressed: ()async{
                      checkInet(context, () async {

                        await send_email(email: email_controller.text, onError: (String e){showError(context, e);}, onResponse:(){
                          Navigator.push(context, MaterialPageRoute(builder: (context) => Verify_OTP(email: email_controller.text)));
                        }, context: context);
                      });

                    },
                  ),
                ),
              ),
              SizedBox(height: 14,),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text("Я вспомнил свой пароль! ",style: Theme.of(context).textTheme.titleSmall!.copyWith(fontWeight: FontWeight.w400),),
                  InkWell(
                    child: Text("Вернуться", style: Theme.of(context).textTheme.titleSmall!.copyWith(color: Color(0xFF7576D6), fontWeight: FontWeight.w700),),
                    onTap: (){ Navigator.push(context, MaterialPageRoute(builder: (context) => Sign_In()));},
                  )
                ],
              )
            ],
          ),
        ),
      ),

    );
  }
}
