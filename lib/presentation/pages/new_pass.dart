import 'package:flutter/material.dart';
import 'package:sec_1_2_new/data/supabase.dart';
import 'package:sec_1_2_new/domain/controllers/password_controller.dart';
import 'package:sec_1_2_new/domain/internet.dart';
import 'package:sec_1_2_new/presentation/pages/log_in.dart';
import 'package:sec_1_2_new/presentation/widgets/dialogs.dart';
import 'package:sec_1_2_new/presentation/widgets/text_field.dart';

class New_pass extends StatefulWidget {
  const New_pass({super.key});


  @override
  State<New_pass> createState() => _New_passState();
}

class _New_passState extends State<New_pass> {
  var password_controller = PasswordController();
  var confirm_password_controller = PasswordController();
  bool password_obscure = true;
  bool conf_password_obscure = true;


  @override
  Widget build(BuildContext context) {

    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 24),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 83,),
              Text(
                "Новый пароль",
                style: Theme.of(context).textTheme.titleMedium,
              ),
              SizedBox(height: 8,),
              Text("Введите новый пароль", style: Theme.of(context).textTheme.titleSmall),
              SizedBox(height: 28,),
              Custom_field(label: 'Пароль', hint: "**********", controller: password_controller, is_obsure: password_obscure, tap_suffix: (){setState(() {
                password_obscure = !password_obscure;
              });},),
              SizedBox(height: 24,),
              Custom_field(label: 'Повторите пароль', hint: "**********", controller: confirm_password_controller, is_obsure: conf_password_obscure, tap_suffix: (){setState(() {
                conf_password_obscure = !conf_password_obscure;
              });},),
              SizedBox(height: 411,),
              Align(
                alignment: Alignment.center,
                child: SizedBox(
                  height: 46,
                  width: 342,
                  child: FilledButton(
                    style: Theme.of(context).filledButtonTheme.style,
                    child: Text(
                      "Подтвердить",
                      style: Theme.of(context).textTheme.titleLarge,
                    ),
                    onPressed: ()async{
                      checkInet(context, () async {
                        if (password_controller.hash_pass() == confirm_password_controller.hash_pass()){
                          await change_pass(pass: password_controller.hash_pass(), onError: (String e){showError(context, e);}, onResponse:(){
                            Navigator.push(context, MaterialPageRoute(builder: (context) => Sign_In()));
                          }, context: context);
                        }
                        else{
                          showError(context, "Passwords don't match");
                        }
                      });

                    },
                  ),
                ),
              ),
              SizedBox(height: 14,),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text("Я вспомнил свой пароль! ",style: Theme.of(context).textTheme.titleSmall!.copyWith(fontWeight: FontWeight.w400),),
                  InkWell(
                    child: Text("Вернуться", style: Theme.of(context).textTheme.titleSmall!.copyWith(color: Color(0xFF7576D6), fontWeight: FontWeight.w700),),
                    onTap: (){ Navigator.push(context, MaterialPageRoute(builder: (context) => Sign_In()));},
                  )
                ],
              )
            ],
          ),
        ),
      ),

    );
  }
}
