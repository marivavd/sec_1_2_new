import 'package:flutter/material.dart';
import 'package:sec_1_2_new/data/supabase.dart';
import 'package:sec_1_2_new/domain/controllers/password_controller.dart';
import 'package:sec_1_2_new/domain/internet.dart';
import 'package:sec_1_2_new/presentation/pages/forgot_pass.dart';
import 'package:sec_1_2_new/presentation/pages/home.dart';
import 'package:sec_1_2_new/presentation/pages/sign_up.dart';
import 'package:sec_1_2_new/presentation/widgets/dialogs.dart';
import 'package:sec_1_2_new/presentation/widgets/text_field.dart';

class Sign_In extends StatefulWidget {
  const Sign_In({super.key});



  @override
  State<Sign_In> createState() => _Sign_InState();
}

class _Sign_InState extends State<Sign_In> {
  var email_controller = TextEditingController();
  var password_controller = PasswordController();
  bool password_obscure = true;
  bool val = false;


  @override
  Widget build(BuildContext context) {

    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 24),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 83,),
              Text(
                "Добро пожаловать",
                style: Theme.of(context).textTheme.titleMedium,
              ),
              SizedBox(height: 8,),
              Text("Заполните почту и пароль чтобы продолжить", style: Theme.of(context).textTheme.titleSmall,),
              SizedBox(height: 28,),
              Custom_field(label: 'Почта', hint: "***********@mail.com", controller: email_controller),
              SizedBox(height: 24,),
              Custom_field(label: 'Пароль', hint: "**********", controller: password_controller, is_obsure: password_obscure, tap_suffix: (){setState(() {
                password_obscure = !password_obscure;
              });},),
              SizedBox(height: 18,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      SizedBox(
                        height: 22,
                        width: 22,
                        child: Checkbox(
                          value: val,
                          onChanged: (value){
                            setState(() {
                              val = value!;
                            });
                          },
                          side: BorderSide(
                            width: 1,
                            color: Color(0xFF5C636A),

                          ),
                          activeColor: Color(0xFF7576D6),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(8)
                          ),
                        ),
                      ),
                      SizedBox(width: 8,),
                      Text(
                        "Запомнить меня",
                        style: Theme.of(context).textTheme.titleSmall,
                      ),

                    ],
                  ),
                  InkWell(
                    onTap: (){ Navigator.push(context, MaterialPageRoute(builder: (context) => Forgot_Pass()));},
                    child: Text('Забыли пароль?',style: Theme.of(context).textTheme.titleSmall!.copyWith(color: Color(0xFF7576D6),
                    )
                    ),
                  )
                ],
              ),
              SizedBox(height: 371,),
              Align(
                alignment: Alignment.center,
                child: SizedBox(
                  height: 46,
                  width: 342,
                  child: FilledButton(
                    style: Theme.of(context).filledButtonTheme.style,
                    child: Text(
                      "Войти",
                      style: Theme.of(context).textTheme.titleLarge,
                    ),
                    onPressed: ()async{
                      checkInet(context, () async {

                          await sign_in(email: email_controller.text, password: password_controller.hash_pass(), onError: (String e){showError(context, e);}, onResponse:(){
                            Navigator.push(context, MaterialPageRoute(builder: (context) => MyHomePage()));
                          }, context: context);
                      });

                    },
                  ),
                ),
              ),
              SizedBox(height: 14,),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text("У меня нет аккаунта! ",style: Theme.of(context).textTheme.titleSmall!.copyWith(fontWeight: FontWeight.w400),),
                  InkWell(
                    child: Text("Создать", style: Theme.of(context).textTheme.titleSmall!.copyWith(color: Color(0xFF7576D6), fontWeight: FontWeight.w700),),
                    onTap: (){ Navigator.push(context, MaterialPageRoute(builder: (context) => Sign_Up()));},
                  )
                ],
              )
            ],
          ),
        ),
      ),

    );
  }
}
