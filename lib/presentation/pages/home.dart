import 'package:flutter/material.dart';
import 'package:sec_1_2_new/data/supabase.dart';
import 'package:sec_1_2_new/domain/internet.dart';
import 'package:sec_1_2_new/presentation/pages/log_in.dart';
import 'package:sec_1_2_new/presentation/widgets/dialogs.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key});


  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {


  @override
  Widget build(BuildContext context) {

    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: SizedBox(
          height: 46,
          width: 342,
          child: FilledButton(
            style: Theme.of(context).filledButtonTheme.style,
            child: Text(
              "ВЫХОД",
              style: Theme.of(context).textTheme.titleLarge,
            ),
            onPressed: ()async{
              checkInet(context, (){
                log_out(onError: (String e){showError(context, e);}, onResponse: (){ Navigator.push(context, MaterialPageRoute(builder: (context) => Sign_In()));}, context: context);
              });

            },
          ),
        ),
      ),
    );
  }
}
