import 'package:flutter/material.dart';
import 'package:sec_1_2_new/domain/theme.dart';
import 'package:sec_1_2_new/presentation/pages/log_in.dart';

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: light_theme,
      darkTheme: dark_theme,
      home: const Sign_In(),
    );
  }
}