

import 'package:flutter/material.dart';

void showError(BuildContext context, String e){
  showDialog(context: context, builder: (BuildContext context) {
    return AlertDialog(
      title: Text("Error"),
      content: Text(e),
      actions: [
        OutlinedButton(onPressed: (){Navigator.of(context).pop();}, child: Text('Ok'))
      ],
    );
  });
}

void showLoading(BuildContext context){
  showDialog(context: context, builder: (BuildContext context){
    return Dialog(
      backgroundColor: Colors.transparent,
      child: Center(child: CircularProgressIndicator(),),
    );
  },
  barrierDismissible: false);
}