
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class Custom_field extends StatelessWidget{
  final String label;
  final String hint;
  final TextEditingController controller;
  final bool is_obsure;
  final Function()? tap_suffix;

  const Custom_field({super.key, required this.label, required this.hint, required this.controller, this.is_obsure = false, this.tap_suffix});




  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(label,
        style: Theme.of(context).textTheme.titleSmall,),
        SizedBox(height: 8,),
        TextField(
          controller: controller,
          obscureText: is_obsure,
          obscuringCharacter: '*',
          decoration: InputDecoration(
            hintText: hint,
            hintStyle: Theme.of(context).textTheme.titleSmall!.copyWith(color: Color(0xFFCFCFCF)),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(4),
              borderSide: BorderSide(width: 1, color: Color(0xFF5C636A)),
            ),
            contentPadding: EdgeInsets.symmetric(vertical: 14, horizontal: 10),
            suffixIcon: (tap_suffix != null) ? GestureDetector(
              onTap: tap_suffix,
              child: Image.asset('assets/eye-slash.png'),
            ):null
          ),
        )
      ],
    );
  }

}