import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:sec_1_2_new/presentation/widgets/dialogs.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

import '../domain/run.dart';

Future<void> sign_up(
{
  required String email,
  required String password,
  required Function(String) onError,
  required Function() onResponse,
  required BuildContext context
}
    )async{
  try{
    showLoading(context);
    final AuthResponse res = await supabase.auth.signUp(
      email: email,
      password: password,
    );
    onResponse();
  }
  on AuthException catch(e){
    Navigator.of(context).pop();
    onError(e.message);
  }
  on Exception catch(e){
    Navigator.of(context).pop();
    onError(e.toString());
  }

}
Future<void> sign_in(
    {
      required String email,
      required String password,
      required Function(String) onError,
      required Function() onResponse,
      required BuildContext context
    }
    )async{
  try{
    showLoading(context);
    final AuthResponse res = await supabase.auth.signInWithPassword(
      email: email,
      password: password,
    );
    onResponse();
  }
  on AuthException catch(e){
    Navigator.of(context).pop();
    onError(e.message);
  }
  on Exception catch(e){
    Navigator.of(context).pop();
    onError(e.toString());
  }

}

Future<void> log_out(
    {
      required Function(String) onError,
      required Function() onResponse,
      required BuildContext context
    }
    )async{
  try{
    showLoading(context);
    await supabase.auth.signOut();
    onResponse();
  }
  on AuthException catch(e){
    Navigator.of(context).pop();
    onError(e.message);
  }
  on Exception catch(e){
    Navigator.of(context).pop();
    onError(e.toString());
  }

}


Future<void> send_email(
    {
      required String email,
      required Function(String) onError,
      required Function() onResponse,
      required BuildContext context
    }
    )async{
  try{
    showLoading(context);
    await supabase.auth.resetPasswordForEmail(email);
    onResponse();
  }
  on AuthException catch(e){
    Navigator.of(context).pop();
    onError(e.message);
  }
  on Exception catch(e){
    Navigator.of(context).pop();
    onError(e.toString());
  }

}



Future<void> verify_OTP(
    {
      required String email,
      required String code,
      required Function(String) onError,
      required Function() onResponse,
      required BuildContext context
    }
    )async{
  try{
    showLoading(context);
    final AuthResponse res = await supabase.auth.verifyOTP(
      type: OtpType.email,
      token: code,
      email: email,
    );
    onResponse();
  }
  on AuthException catch(e){
    Navigator.of(context).pop();
    onError(e.message);
  }
  on Exception catch(e){
    Navigator.of(context).pop();
    onError(e.toString());
  }

}


Future<void> change_pass(
    {
      required String pass,
      required Function(String) onError,
      required Function() onResponse,
      required BuildContext context
    }
    )async{
  try{
    showLoading(context);
    final UserResponse res = await supabase.auth.updateUser(
      UserAttributes(
        password: pass
      ),
    );
    onResponse();
  }
  on AuthException catch(e){
    Navigator.of(context).pop();
    onError(e.message);
  }
  on Exception catch(e){
    Navigator.of(context).pop();
    onError(e.toString());
  }

}