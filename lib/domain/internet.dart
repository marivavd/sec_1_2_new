
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:sec_1_2_new/presentation/widgets/dialogs.dart';

Future<void> checkInet(BuildContext context, Function onGood)async{
  var con_res = await Connectivity().checkConnectivity();
  if (con_res != ConnectivityResult.none){
    onGood();
  }
  else{
    showError(context, "No Internet");
  }
}
